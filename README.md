# FreeAlias

This is a tiny application that on visit gives you a random first name, last name, fulln ame and email which you can use to have more anonymous/private accounts. It was created specifically for me but others might find it useful. You can view it live [here](https://freealias.fab1an.dev/).

As an extra feature you can set your own domain and the app saves that in the most secure cookie possible on your system. Which the application on following visits appends to the randomly generated email.

This application can easily be self-hosted with its Dockerfile, unfortunately I can't share my builds. The names it gets from a local sqlite database, you have to seed that yourself.
