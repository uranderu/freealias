-- name: GetRandomFirstName :one
SELECT name
FROM first_names
WHERE ROWID = (ABS(RANDOM()) % (SELECT MAX(ROWID) FROM first_names)) + 1;

-- name: GetRandomLastName :one
SELECT name
FROM last_names
WHERE ROWID = (ABS(RANDOM()) % (SELECT MAX(ROWID) FROM last_names)) + 1;
