package database

import (
	"database/sql"
	db "gitlab.com/uranderu/free-alias/internal/database/gen"
	"log"

	_ "modernc.org/sqlite"
)

type Service struct {
	Queries *db.Queries
}

var dbInstance *Service

func New() *Service {
	// Reuse Connection
	if dbInstance != nil {
		return dbInstance
	}

	conn, err := sql.Open("sqlite", "db/freeAlias.db?immutable=1")
	if err != nil {
		log.Fatal(err)
	}

	queries := db.New(conn)

	dbInstance = &Service{
		Queries: queries,
	}
	return dbInstance
}
