package app

import (
	"gitlab.com/uranderu/free-alias/internal/database"
	"html/template"
	"log"
	"net/http"
)

type Server struct {
	Router   *http.ServeMux
	Template *template.Template
	Db       *database.Service
}

func (s *Server) registerRoutes() {
	dir := http.Dir("./assets")
	fs := http.FileServer(dir)

	s.Router.Handle("GET /assets/", http.StripPrefix("/assets", fs))

	s.Router.HandleFunc("GET /health", s.healthHandler)

	s.Router.HandleFunc("GET /", s.aliasHandler)
	s.Router.HandleFunc("POST /set-domain", s.setDomainHandler)
}

func New() *Server {
	indexTemplate, err := template.ParseFiles("./templates/index.html")
	if err != nil {
		log.Fatal(err)
	}

	router := http.NewServeMux()

	server := &Server{
		Router:   router,
		Template: indexTemplate,
		Db:       database.New(),
	}

	server.registerRoutes()

	return server
}
