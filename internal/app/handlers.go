package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

const cookieName = "__Host-domain"

type Alias struct {
	FirstName string
	LastName  string
	FullName  string
	Email     string
}

func handleError(w http.ResponseWriter) {
	http.Error(w, "There is an error on the server. There's nothing you can do. Please try again later.", http.StatusInternalServerError)
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := w.Write([]byte("OK"))
	if err != nil {
		handleError(w)
	}
}

func (s *Server) aliasHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	firstName, err := s.Db.Queries.GetRandomFirstName(ctx)
	if err != nil {
		log.Printf("Error getting first name: %v", err)
		handleError(w)
	}

	lastName, err := s.Db.Queries.GetRandomLastName(ctx)
	if err != nil {
		log.Printf("Error getting last name: %v", err)
		handleError(w)
	}

	fullName := fmt.Sprintf("%s %s", firstName, lastName)

	emailPrefix := getRandomEmailPrefix()

	var emailSuffix string
	emailCookie, err := r.Cookie(cookieName)
	if err != nil {
		emailSuffix = ""
	} else {
		emailSuffix = emailCookie.Value
	}

	email := fmt.Sprintf("%s@%s", emailPrefix, emailSuffix)

	alias := Alias{
		FirstName: firstName,
		LastName:  lastName,
		FullName:  fullName,
		Email:     email,
	}

	w.Header().Set("Content-Type", "text/html")

	err = s.Template.Execute(w, alias)
	if err != nil {
		log.Printf("Error executing template: %v", err)
		handleError(w)
	}
}

func (s *Server) setDomainHandler(w http.ResponseWriter, r *http.Request) {
	domain := r.FormValue("domain")

	cookie := &http.Cookie{
		Name:     cookieName,
		Value:    domain,
		Path:     "/",
		MaxAge:   1707109200,
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}

	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/", http.StatusSeeOther)
}
