package app

import (
	"crypto/rand"
	"log"
	"math/big"
)

const allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789"
const length = 16

func getRandomEmailPrefix() string {
	randomBytes := make([]byte, length)

	// Generate random bytes using crypto/rand
	for i := range length {
		index, err := rand.Int(rand.Reader, big.NewInt(int64(len(allowedChars))))
		if err != nil {
			log.Fatal(err)
		}
		randomBytes[i] = allowedChars[index.Int64()]
	}

	randomString := string(randomBytes)

	return randomString
}
