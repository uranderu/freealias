FROM golang:1.23-alpine AS builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download
RUN go mod verify

COPY . .

RUN go build -o freeAlias cmd/main.go

FROM alpine:latest

WORKDIR /app

COPY --from=builder /app/freeAlias .

COPY templates ./templates
COPY assets ./assets
# DB Driver needs directory to already exist.
COPY db ./db

EXPOSE 8080

CMD ["./freeAlias"]
