package main

import (
	"fmt"
	"gitlab.com/uranderu/free-alias/internal/app"
	"log"
	"net/http"
	"os"
)

func main() {
	server := app.New()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	log.Printf("Server listening on port %s", port)
	err := http.ListenAndServe(fmt.Sprintf(":%s", port), server.Router)
	if err != nil {
		log.Fatal(err)
	}
}
